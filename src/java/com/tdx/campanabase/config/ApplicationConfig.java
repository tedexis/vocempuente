/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdx.campaigntools.servicesdatahelper.jsonSender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Application;

/**
 * Esto se ejecutara cuando la campana sea subida a wildfly actualizando su
 * cache automaticamente al subir. dede el modatamaster.
 *
 * @author erodriguez
 */
@javax.ws.rs.ApplicationPath("/")
public class ApplicationConfig extends Application {

    //quizas parametrizable >:c
    //OJO 8083
    public static int campaignCode = 17;
    public static String informationendpoint = "http://localhost:8083/modatamaster/services/campaigninfo";
    public static boolean autorefresh = true;
    public static jsonSender jsonSenderCampaignData;
    public static HashMap<String, String> mp;
    public static HashMap<String, String> mensajesRespuesta;

    public ApplicationConfig() {
        this.getInfo();

    }

    @Override
    public Set<Class<?>> getClasses() {
        return super.getClasses(); //To change body of generated methods, choose Tools | Templates.
    }

    public static void getInfo() {
        try {

            String finalUrl = informationendpoint;

            URL urlObject = new URL(finalUrl);

            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObject.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; ");
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setDoOutput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            String ab = "{'idcampana':" + campaignCode + "}";
            outputStream.write(ab.getBytes());

            outputStream.flush();

            outputStream.close();

//            System.out.println(httpURLConnection.getResponseCode());
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            in.close();

            JsonElement jelement = new JsonParser().parse(response.toString());
            JsonObject jobject = jelement.getAsJsonObject();
            jobject = jobject.getAsJsonObject("data");
            Gson gson=  new GsonBuilder().setDateFormat("MMM dd, yyyy hh:mm:ss a").create();
            jsonSender respuesta = gson.fromJson(jobject.toString(), jsonSender.class);

            mp = respuesta.cpjson();
            mensajesRespuesta = respuesta.cmjson();
            jsonSenderCampaignData = respuesta; 
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ApplicationConfig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationConfig.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static class sendjson {

        public int idcampana = campaignCode;
    }

}
