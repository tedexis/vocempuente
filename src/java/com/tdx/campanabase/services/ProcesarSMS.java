/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.campanabase.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.tdx.campaigntools.objects.SMSRequest;
import com.tdx.campaigntools.servicesdatahelper.jsonSender;
import static com.tdx.campanabase.config.ApplicationConfig.campaignCode;
import static com.tdx.campanabase.config.ApplicationConfig.informationendpoint;
import static com.tdx.campanabase.config.ApplicationConfig.mensajesRespuesta;
import static com.tdx.campanabase.config.ApplicationConfig.mp;
import com.tedexis.utils.response.ResponseService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static com.tdx.campanabase.config.ApplicationConfig.jsonSenderCampaignData;
import static com.tdx.campanabase.config.ApplicationConfig.autorefresh;

/**
 *
 * @author erodriguez
 */
@Path("services")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProcesarSMS {
          private final ResponseService responseService = new ResponseService();
          private static Logger Logger;

    @Context
    @GET
    @Path("refresh")
    public String refreshCampaign() {

        try {
            String finalUrl = informationendpoint;

            URL urlObject = new URL(finalUrl);

            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObject.openConnection();

            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setRequestProperty("Content-Type", "application/json; ");

            httpURLConnection.setConnectTimeout(10000);

            httpURLConnection.setDoOutput(true);

            OutputStream outputStream = httpURLConnection.getOutputStream();

            String ab = "{'idcampana':" + campaignCode + "}";
            outputStream.write(ab.getBytes());

            outputStream.flush();

            outputStream.close();

//            System.out.println(httpURLConnection.getResponseCode());
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            in.close();

            JsonElement jelement = new JsonParser().parse(response.toString());
            JsonObject jobject = jelement.getAsJsonObject();
            jobject = jobject.getAsJsonObject("data");
Gson gson=  new GsonBuilder().setDateFormat("MMM dd, yyyy hh:mm:ss a").create();
            jsonSender respuesta = gson.fromJson(jobject.toString(), jsonSender.class);

//
            mp = respuesta.cpjson();
            mensajesRespuesta = respuesta.cmjson();
            
             jsonSenderCampaignData = respuesta; 
             
            return "ACTUALIZADO";
        } catch (IOException ex) {
            Logger.getLogger(ProcesarSMS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    
    
    
    /**
     * Metodo response llamar manejar un mensaje
   result = appHelper.webServiceInvoke(channel.appurl.trim(), channel.appkey.trim(), country_code, cod_area, local, text.trim(), env.getId().trim());
     * @param json
     * @return 
     */
     @POST
    @Path("onmessage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
     
    public Response onmessage(String json) {
//         mopData recievej= null;
          SMSRequest smsrequest=null;
      
         try{ 
       //Agregado, ahora la informacion de la campana esta en el jsonSenderCampaignData que se actualiza
       // con cada REFRESH. (POST)
           autorefresh(); 

       
        mopResponse response = new mopResponse();
        //Para saber si esta activa o inactiva
       if(jsonSenderCampaignData.status==0)
       { 
           response.Mensaje=jsonSenderCampaignData.inactive_msg;
           System.out.println("Thread:"+Thread.currentThread().getId() + " VOCEM:" + "Respuesta a enviar:  "+response.Mensaje );
           return responseService.responseSuccess(new Gson().toJson(response, mopResponse.class)); 
       }
       //para validar el tiempo.
       if(!jsonSenderCampaignData.dateValid())
       {
                      response.Mensaje=jsonSenderCampaignData.end_msg;
                      System.out.println("Thread:"+Thread.currentThread().getId() + " VOCEM:" + "Respuesta a enviar:  "+response.Mensaje );
                      
           return responseService.responseSuccess(new Gson().toJson(response, mopResponse.class)); 
       }
       
       
       
        
               smsrequest = new Gson().fromJson(json, SMSRequest.class);
          
        System.out.println("Thread:"+Thread.currentThread().getId() +" VOCEM: Recibiendo llamado con payload: " +json);
        }catch (JsonSyntaxException e){
            System.out.println("ERROR:  "+e );
        }

        if (smsrequest == null ) {
            
            mopResponse response = new mopResponse();
              response.Mensaje=jsonSenderCampaignData.error_msg;
               return responseService.responseSuccess(new Gson().toJson(response, mopResponse.class)); 
//            return responseService.responseFail();
        }     
         System.out.println("Thread:"+Thread.currentThread().getId() + " VOCEM: Recibiendo mensaje: "+smsrequest.getNumero()+" Texto: "+smsrequest.getTexto()+ " id: "+smsrequest.getIdMensaje());     
         mopResponse response = new mopResponse();
         
         try {  
             response.Mensaje= (String) mensajesRespuesta.get("mensaje_success");
         } catch (Exception e) {
             //NOTA: este try catch creo q no esta haciendo nada ya que el metodo .process tiene sus propios try catch y retornan una respuesta
             //en caso de error. (SE MODIFICARON ESOS)
//              response.Mensaje= mp.get("mensage_error");
                response.Mensaje=jsonSenderCampaignData.error_msg;
              System.out.println("Thread:"+Thread.currentThread().getId() + " VOCEM:" + "ERROR:  "+e );
         }
         System.out.println("Thread:"+Thread.currentThread().getId() + "VOCEM:" + "Respuesta a enviar:  "+response.Mensaje );
         return responseService.responseSuccess(new Gson().toJson(response, mopResponse.class));
    }
 
    
    public class mopData
    {
        public String country_code;
        public String cod_area;
        public String local;
        public String text;
        public String id;
    }
    
    public class mopResponse
    {
        public String Mensaje = "";
    }
    
     private void autorefresh() {
         if(autorefresh){
        this.refreshCampaign();
        }
     }
}
